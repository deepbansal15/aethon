CMAKE_MINIMUM_REQUIRED(VERSION 3.5)
project(aethon)

SET(CMAKE_CXX_STANDARD 11)
SET(CMAKE_CXX_EXTENSIONS OFF)

# Add embree 
# find_package(Embree 3.5.2 REQUIRED PATHS embree/)
# include_directories(${EMBREE_INCLUDE_DIRS})
# message(STATUS "Found Embree in ${EMBREE_ROOT_DIR}")
# ${EMBREE_LIBRARIES}

# Add GLM
find_package(GLM REQUIRED)
include_directories(${GLM_INCLUDE_DIRS})
message(STATUS "Found GLM in ${GLM_DIR}")

add_definitions(-DUSE_EMBREE)
# Add source code
set(SOURCE src/main.cpp src/memory/tlsf.c)

add_executable(${PROJECT_NAME} ${SOURCE})
target_link_libraries(${PROJECT_NAME})