#include <embree3/rtcore.h>
#include <glm/glm.hpp>

// RTCScene scene;

int main()
{
    // RTCGeometry geometry;
    // rtcAttachGeometry(scene,geometry);
    return 0;
}

/// TODO : Include glfw, glm

/// Represents transform
struct Transform {};

/// Represents the final image
struct Film {};

/// Medium that the camera is inside
struct Medium {};

struct CameraSample {
    glm::vec2 pfilm;
    glm::vec2 plens;
    float time;
};

struct Ray {};

struct Camera
{
    float GenerateRay(const CameraSample& sample,Ray* ray);
    const float shutter_open,shutter_close;
    Film* film; 
    Transform camera_to_world;
    const Medium* medium;
};

///----------
/// Sample policy design
template<class policy>
struct Logger
{
    void log(){p.filter();}
    policy p;
};

struct ConsolePolicy
{
    void filter(){}
};

struct FreePolicy
{
    void filter(){}
};

Logger<ConsolePolicy> logger;
Logger<FreePolicy> free_logger;
///----------