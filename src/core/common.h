#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdint.h>
#include <float.h>

#ifdef USE_EMBREE
#define AT_EMBREE
#endif


#ifdef AETHON_NAMESAPCE
#define AT_NAMESPACE_BEGIN namespace aethon{
#define AT_NAMESPACE_END }
#else
#define AT_NAMESPACE_BEGIN
#define AT_NAMESPACE_END
#endif

#ifdef _WIN32
#define AT_ALIGN(...) __declspec(align(__VA_ARGS__))
#else
#define AT_ALIGN(...) __attribute__((aligned(__VA_ARGS__)))
#endif

typedef uint32_t uint32;
typedef int32_t int32;
typedef int64_t int64;
typedef float float32;
typedef double float64;

#endif