#ifndef __GEOMETRY_H_
#define __GEOMETRY_H_

#include "common.h"

#include <embree3/rtcore_ray.h>
#include <glm/glm.hpp>

AT_NAMESPACE_BEGIN

typedef glm::vec3 point3f;
typedef glm::vec2 point2f;
typedef glm::i32vec2 point2i;
typedef glm::i32vec3 point3i;

struct Ray
{
    Ray():o(glm::vec3(0)),tnear(0),d(glm::vec3(0)),t(0),tfar(0),mask(0),id(0),flags(0){}
    Ray(const glm::vec3& o,const glm::vec3& d,float tnear,float tfar):o(o),d(d),tnear(tnear),
        tfar(tfar),t(t){}
    point3f operator()(float t){ o + t * d; }
    union 
    {
        struct AT_ALIGN(16) 
        {
            glm::vec3 o;
            float tnear;
            glm::vec3 d;
            float t;
            float tfar;
            uint32 mask;
            uint32 id;
            uint32 flags;
        };
#ifdef  AT_EMBREE
        RTCRay _ray;
#endif
    };
};

AT_NAMESPACE_END

#endif